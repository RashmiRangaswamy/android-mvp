package com.example.mvp.presenter;

import com.example.mvp.contract.IMainContract;

public class MainPresenter implements IMainContract.IMainPresenter {

    private static final String TAG = "MainPresenter";

    private IMainContract.IMainView view;


    public MainPresenter(IMainContract.IMainView mainView) {
        this.view = mainView;
    }


    @Override
    public void setView(IMainContract.IMainView view) {
            this.view = view;
    }

    @Override
    public void saveButtonClicked() {
        if (view != null)
            view.dataSaved();
    }

}
