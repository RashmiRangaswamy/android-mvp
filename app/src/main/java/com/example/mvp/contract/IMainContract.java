package com.example.mvp.contract;


import com.example.mvp.presenter.IBasePresenter;
import com.example.mvp.view.IBaseView;

public interface IMainContract {

    interface IMainPresenter extends IBasePresenter {
        void setView(IMainView view);
        void saveButtonClicked();
    }

    interface IMainView extends IBaseView<IMainPresenter> {
        void dataSaved();
    }

}
