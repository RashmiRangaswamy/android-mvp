package com.example.mvp.view;

import com.example.mvp.presenter.IBasePresenter;

/* this interface should be implemented by all the views in your app.
    Since View interact with the presenter, the view is given a generic type
    P for the presenter and they must all contain a setPresenter() method.
*/
public interface IBaseView<P extends IBasePresenter> {
        void setPresenter(P presenter);
}
