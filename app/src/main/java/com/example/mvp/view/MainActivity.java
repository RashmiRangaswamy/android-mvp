package com.example.mvp.view;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.example.mvp.R;
import com.example.mvp.contract.IMainContract;
import com.example.mvp.presenter.MainPresenter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        IMainContract.IMainView {

    IMainContract.IMainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainPresenter = new MainPresenter(this);
        initUi();
    }

    private void initUi() {
        EditText editTextFirstName = findViewById(R.id.first_name);
        EditText editTextLastName = findViewById(R.id.last_name);
        EditText editTextEmail = findViewById(R.id.email);
        EditText editTextMobileNumber = findViewById(R.id.mobile_number);
        FloatingActionButton saveButton = findViewById(R.id.save_button);
        saveButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.save_button) {
            mainPresenter.saveButtonClicked();
        }
    }

    @Override
    public void dataSaved() {
        Toast.makeText(this, "Data saved", Toast.LENGTH_LONG).show();
    }

    @Override
    public void setPresenter(IMainContract.IMainPresenter presenter) {
        this.mainPresenter = presenter;
    }
}
